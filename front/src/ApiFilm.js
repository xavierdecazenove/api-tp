const axios = require('axios');


const API_URL = 'http://localhost:3000';




export default class ApiFilm  {
    getFilms(){
        return axios.get(`${API_URL}/films`);
    }
    getFilmByid(id){
        return axios.get(`${API_URL}/films/${id}`);
    }
    deleteById(id){
        return axios.delete(`${API_URL}/films/${id}`);
    }
}