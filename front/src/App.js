import React, {Component} from 'react';
import './App.css';
import Card from 'react-bootstrap/Card';
import ApiFilm from './ApiFilm';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from "react-bootstrap/Button";


class CardLinkJSON extends Component{

    render() {
        return(
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={this.props.value.poster} />
                <Card.Body>
                    <Card.Title>{this.props.value.movie}</Card.Title>
                    <Card.Text>
                        {this.props.value.yearOfRelease} - {this.props.value.actors} // ID : {this.props.value.id}
                    </Card.Text>
                    <Button variant="primary" onClick={this.props.onClick}>Delete</Button>
                </Card.Body>
            </Card>
        );
    }
}

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            films: []
        }
    }

    componentDidMount(){
        this.requestAllFilms();
    }

    requestAllFilms() {
        let filmsJSON = new ApiFilm();
        filmsJSON.getFilms().then(response => {
            console.log(response.data.films);
            this.setState({films: response.data.films})
        });
    }

    delete(film){
        let filmsJSON = new ApiFilm();
        filmsJSON.deleteById(film.id);
        this.requestAllFilms();
    }

    render() {
        return (
            <Container>
                <Row>
                {
                    this.state.films.map(film =>
                        <Col xs={4}>
                            <CardLinkJSON value={film} onClick={() => this.delete(film)}/>
                        </Col>
                        )
                }
                </Row>
            </Container>
        );
    }
}

export default App;
