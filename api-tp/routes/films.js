const express = require('express');
// Lodash utils library
const _ = require('lodash');
const axios = require('axios');
const data = require('../data.json');
const fs = require("fs");

const router = express.Router();


const API_KEY = "a1300a30";


// Permet de réécrire les données dans le fichier JSON
function updateJSON(films) {
    let dataFilms = JSON.stringify(films, null, 2);
    fs.writeFileSync('data.json', dataFilms);
}

// Permet de récupérer les données du fichier JSON
function readJSON() {
    return JSON.parse(fs.readFileSync('data.json'));
}



/* GET films listing. */
router.get('/', (req, res) => {
    // Récupération de l'ensemble des films du fichier JSON
    let films = readJSON();
    res.status(200).json({films});
});


/* GET one film. */
router.get('/:id', (req, res) => {
    const id = req.params.id;
    let film = readJSON();
    if (_.find(film, ["id", id])) {
        res.status(200).json({
            message: 'Film found!',
            film
        });
    } else {
        res.status(200).json({
            message: 'Film not found...'
        });
    }
});


/* PUT new user. */
router.post('/:movie', (req, res) => {
    // Make a request for a user with a given ID
    axios.get(`http://www.omdbapi.com/?t=${req.params.movie}&apikey=${API_KEY}`)
        .then(function (response) {
            if (response.data.Title != null) {
                // Récupération de l'ensemble des films du fichier JSON
                let films = readJSON();

                // handle success
                let film = {
                    id: response.data.imdbID,
                    movie: response.data.Title,
                    yearOfRelease: parseInt(response.data.Year),
                    duration: response.data.Runtime, // en minutes,
                    actors: response.data.Actors,
                    poster: response.data.Poster, // lien vers une image d'affiche,
                    boxOffice: response.data.BoxOffice, // en USD$,
                    rottenTomatoesScore: parseFloat(response.data.imdbRating)
                };

                films.push(film);
                updateJSON(films);

                res.status(200).json({
                    message: "Film add :",
                    film
                });
            } else {
                res.status(200).json({
                    message: "No film with this name...",
                });
            }
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
});


/* DELETE user. */
router.delete('/:id', (req, res) => {
    // Get the :id of the user we want to delete from the params of the request
    const {id} = req.params;

    // Récupération de l'ensemble des films du fichier JSON
    let films = readJSON();

    // Remove from "DB"
    _.remove(films, ["id", id]);

    // Réécrit le JSON
    updateJSON(films);

    // Return message
    res.json({
        message: `Just removed ${id}`
    });
});


/* UPDATE user. */
router.put('/:id', (req, res) => {
    // Get the :id of the user we want to update from the params of the request
    const {id} = req.params;
    // Get the new data of the user we want to update from the body of the request
    const {rating} = req.body;

    // Récupération de l'ensemble des films du fichier JSON
    let films = readJSON();

    // Find in DB
    if (_.find(films, ["id", id])) {
        for (let i = 0; i < films.length; i++) {
            if (films[i].id == id) {
                films[i].rottenTomatoesScore = parseFloat(rating);
                updateJSON(films);
                // Message pour POSTMAN
                res.json({
                    message: `Just updated ${id} with ${rating}`
                });
            }
        }
    } else {
        // Return message
        res.json({
            message: `Error trying to update ${id} film : ${rating}`
        });
    }
});

module.exports = router;